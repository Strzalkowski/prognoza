
START TRANSACTION;

CREATE TABLE uzytkownicy (
                                iduzytkownika SERIAL,
                                nazwa  varchar(64),
                                haslo  varchar(64),
                                email  varchar(256),
                                data_utworzenia  timestamp(0) DEFAULT CURRENT_TIMESTAMP,
                                ost_aktywnosc  timestamp(0),
                                PRIMARY KEY ( iduzytkownika )
);

CREATE TABLE tworzone_konta (
                                   nazwa  varchar(64),
                                   haslo  varchar(64),
                                   email  varchar(256),
                                   data_wyslania_emaila  timestamp(0),
                                   kod_w_emailu  varchar(16),
                                   PRIMARY KEY ( nazwa )
);

CREATE TABLE  sesje  (
                          sid  char(33),
                          iduzytkownika  int NOT NULL,
                          ip  varchar(64),
                          poczatek  timestamp(0) DEFAULT CURRENT_TIMESTAMP,
                          ost_aktywnosc  timestamp(0) DEFAULT CURRENT_TIMESTAMP,
                          user_agent  varchar(1024),
                          PRIMARY KEY ( sid )
);

CREATE TABLE punkty_pogody  (
                                  idsystemu  int NOT NULL,
                                  x  double precision NOT NULL,
                                  y  double precision NOT NULL,
                                  czas_punktu timestamp(0) NOT NULL,
                                  czas_generacji timestamp(0) NOT NULL,
                                  temperatura real,
                                  temperatura_odczuwalna real,
                                  cisnienie_zreduk real,
                                  cisnienie_rzeczyw real,
                                  wilgotnosc real,
                                  opady real,
                                  predkosc_wiatru real,
                                  kierunek_wiatru real,
                                  opis VARCHAR(256),
                                  PRIMARY KEY (x, y, czas_punktu)
);

--abs dla interval, bo z jakiegoś powodu go nie ma...
CREATE OR REPLACE FUNCTION abs(interval) RETURNS interval AS
$$ select case when ($1<interval '0') then -$1 else $1 end; $$
LANGUAGE sql immutable;

--SELECT idsystemu, x,y, czas_punktu, czas_generacji, temperatura, temperatura_odczuwalna, cisnienie_zreduk, cisnienie_rzeczyw, wilgotnosc, opady, predkosc_wiatru, kierunek_wiatru, opis FROM punkty_pogody;
--SELECT abs(czas_punktu - CURRENT_TIMESTAMP) AS odl, idsystemu, x,y, czas_punktu, czas_generacji, temperatura, temperatura_odczuwalna, cisnienie_zreduk, cisnienie_rzeczyw, wilgotnosc, opady, predkosc_wiatru, kierunek_wiatru, opis FROM punkty_pogody
--WHERE x = 20 AND y = 50 AND odl < '4 hours'::interval AND abs(CURRENT_TIMESTAMP - czas_generacji) < '4 hours'::interval ORDER BY odl ASC LIMIT 1
--
CREATE TABLE systemy_pogodowe  (
                                idsystemu  int NOT NULL,
                                nazwa varchar(32),
                                info text
);

CREATE TABLE slady (
                                idsladu SERIAL,
                                idwlasciciela int,
                                nazwa varchar (256),
                                data_dodania timestamp (0) DEFAULT CURRENT_TIMESTAMP,
                                punkty text,
                                widocznosc_publiczna boolean,
                                kopiowanie_dozwolone boolean,
                                PRIMARY KEY (idsladu)
);
--np.:
--INSERT INTO slady (idsladu, idwlasciciela, nazwa, punkty)
--VALUES (1,1,'nic','["lat":[],"lon":[], "elev":[]]');

CREATE TABLE konfiguracje (
                                idkonfiguracji SERIAL,
                                nazwakonfiguracji varchar(256),
                                idsladu int,
                                idwlasciciela int,
                                data_dodania timestamp(0) DEFAULT CURRENT_TIMESTAMP,
                                ost_modyfikacja timestamp(0) DEFAULT CURRENT_TIMESTAMP,
                                nr_punktu_poczatkowego int DEFAULT 0,
                                data_poczatku timestamp(0) DEFAULT NULL,
                                srednia_predkosc_kmh real DEFAULT 15.0,
                                tu boolean,
                                teraz boolean,
                                widocznosc_publiczna boolean DEFAULT false,
                                kopiowanie_dozwolone boolean DEFAULT false,
                                typ_opcj_param int,
                                opcj_param text,
                                postoje text,
                                PRIMARY KEY (idkonfiguracji),
                                FOREIGN KEY (idsladu) REFERENCES slady(idsladu)
                                    ON DELETE CASCADE --raczej rozwiązanie tymczasowe
);

COMMIT;


CREATE PROCEDURE usun_sesje_starsze_niz(IN czas INTERVAL)
AS
$$
BEGIN
    DELETE FROM sesje
    WHERE (CURRENT_TIMESTAMP - ost_aktywnosc) > czas;
END;
$$
LANGUAGE plpgsql;

CREATE PROCEDURE usun_sesje_starsze_niz(IN czas INTERVAL, IN _iduzytkownika INT)
AS
$$
BEGIN
    DELETE FROM sesje
    WHERE (CURRENT_TIMESTAMP - ost_aktywnosc) > czas AND iduzytkownika = _iduzytkownika;
END;
$$
LANGUAGE plpgsql;


--CALL usun_sesje_starsze_niz('1 day');

---------------------------------------------------------------------------------------------------------------------
-- Na podstawie jsona (tekstu) tworzy ślad z tymże Jsonem w środku, o zadanej _nazwie, posiadany przez _wlasciciela
-- podmienic = true -> jeśli znajdzie konfigurację z tę samą nazwą, kasuje i dodaje nowa (stary ślad zostaje)
CREATE PROCEDURE dodajKonfiguracje(IN _json TEXT, IN _nazwa VARCHAR(256), IN _idwlasciciela INT, IN podmienic BOOLEAN)
AS
$$
DECLARE _idsladu INT;
BEGIN
    IF podmienic THEN
        DELETE FROM konfiguracje WHERE nazwakonfiguracji = _nazwa;
        --zostawia to osierocone ślady, ale nie chce mi sie na razie tym zajmować...
        --zresztą może być to nawet uzyteczne, zostawić poprzednią wersje śladu, gdyby użytkownik chciał ją przywrócić
    END IF;

    --WSTAW ŚLAD ORAZ KONFIGURACJĘ WSKAZUJĄCĄ NA NIEGO
    SELECT nextval(pg_get_serial_sequence('slady', 'idsladu')) INTO _idsladu;

    INSERT INTO slady (idsladu, idwlasciciela, nazwa, punkty)
    VALUES (_idsladu,_idwlasciciela,_nazwa,_json);

    INSERT INTO konfiguracje (nazwakonfiguracji, idsladu, idwlasciciela, nr_punktu_poczatkowego, data_poczatku, srednia_predkosc_kmh, tu, teraz)
    VALUES(_nazwa,_idsladu,_idwlasciciela,0,CURRENT_TIMESTAMP,15,true,true);

END;
$$
LANGUAGE plpgsql;

--CALL dodajKonfiguracje('[]', 'dupa', 1, false);

CREATE FUNCTION prawo_widoku_konfiguracji(_idkonfiguracji INT, _sid VARCHAR(33))
RETURNS BOOLEAN
AS
$$
DECLARE _iduzytkownika INT;
DECLARE odp BOOLEAN;
BEGIN
    IF _sid ILIKE 'foaloke' THEN RETURN TRUE; END IF;--tak, to jest tylna furtka

    SELECT iduzytkownika INTO _iduzytkownika
    FROM sesje WHERE sid = _sid;

    SELECT (idwlasciciela = _iduzytkownika) INTO odp
    FROM konfiguracje WHERE idkonfiguracji = _idkonfiguracji;

    IF odp = TRUE THEN RETURN TRUE;
    ELSE RETURN FALSE;
    END IF;
END;
$$
LANGUAGE plpgsql;
--SELECT prawo_widoku_konfiguracji(1, '.....');

START TRANSACTION;
CREATE TABLE kluczeOWM(
    klucz VARCHAR(1024) PRIMARY KEY,
    idwlasciciela INT DEFAULT NULL, --NULL znaczy, że klucz jest wspólny, dostępny dla wszystkich
    --licznik INT NOT NULL DEFAULT 0,
    ostatnie_uzycie TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    licznik_minutowy INT NOT NULL DEFAULT 0,
    limit_minutowy INT NOT NULL DEFAULT 60,
    licznik_dzienny INT NOT NULL DEFAULT 0,
    limit_dzienny INT NOT NULL DEFAULT 1000,
    działa BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE OR REPLACE FUNCTION DAJ_KLUCZ_DO_OWM(_sid VARCHAR(255))
RETURNS VARCHAR(1024)
AS
$$
DECLARE _iduzytk INT;
DECLARE _klucz VARCHAR(1024);
BEGIN
    SELECT iduzytkownika INTO _iduzytk
    FROM sesje WHERE sid = _sid;

    IF _iduzytk IS NULL THEN RETURN NULL;
    END IF;

    UPDATE kluczeOWM SET licznik_minutowy = 0 WHERE date_part('minute', ostatnie_uzycie) <> date_part('minute', CURRENT_TIMESTAMP);
    UPDATE kluczeOWM SET licznik_dzienny = 0 WHERE date_part('day', ostatnie_uzycie) <> date_part('day', CURRENT_TIMESTAMP);

    --znajdź jakiś dozwolony klucz
    SELECT klucz INTO _klucz
    FROM (
             (
                 SELECT klucz
                 FROM kluczeOWM
                 WHERE idwlasciciela = _iduzytk
                   AND licznik_minutowy < limit_minutowy
                   AND  licznik_dzienny < limit_dzienny
                   AND działa IS TRUE
                 ORDER BY licznik_minutowy ASC, licznik_dzienny ASC
            )
             UNION
            (
                SELECT klucz
                FROM kluczeOWM
                WHERE idwlasciciela IS NULL
                  AND licznik_minutowy < limit_minutowy
                  AND  licznik_dzienny < limit_dzienny
                  AND działa IS TRUE
                ORDER BY licznik_minutowy DESC, licznik_dzienny ASC
            )
    ) AS dozwoloneklucze
    LIMIT 1;

    IF _klucz IS NULL THEN RETURN NULL; END IF;

    --jeśli jakiś się znalazł, podnieś na nim liczniki i zwróc
    UPDATE kluczeOWM
    SET licznik_minutowy = licznik_minutowy + 1, licznik_dzienny = licznik_dzienny + 1
    WHERE klucz = _klucz;
    RETURN _klucz;
END;
$$
LANGUAGE plpgsql;

INSERT INTO kluczeOWM (klucz, idwlasciciela)
VALUES
       ('667bf4a1b71f43c43c8ad1efdd778a72', NULL);--,
--       ('ZAŚLEPKA', NULL);--spowoduje pobieranie pliku forecast.json zamiast prawdziwej pogody
COMMIT;

START TRANSACTION;
-- źródła danych pogodowych wpisane na twardo

INSERT INTO systemy_pogodowe (idsystemu, nazwa) VALUES
(1,'openweathermap'),
(2, 'icm');


-- testowy użytkownik

INSERT INTO uzytkownicy (nazwa, haslo, email) VALUES
('TheodenKing', 'foaloke', 'kinemaprognoza@interia.pl'),
('test', 'foaloke', 'kinemaprognoza@interia.pl');


COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Sep 12, 2020 at 11:53 PM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

--SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
--SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testdb1`
--

-- --------------------------------------------------------

--
-- Table structure for table `USER`
--

--CREATE TABLE films (
--    code        char(5) CONSTRAINT firstkey PRIMARY KEY,
--    title       varchar(40) NOT NULL,
--    did         integer NOT NULL,
--    date_prod   date,
--    kind        varchar(10),
--    len         interval hour to minute
--);

CREATE TABLE uzytkownicy (
                                iduzytkownika int NOT NULL,
                                nazwa  varchar(64),
                                haslo  varchar(64),
                                email  varchar(256),
                                data_utworzenia  timestamp(0),
                                ost_aktywnosc  timestamp(0),
                                PRIMARY KEY ( iduzytkownika )
);

CREATE TABLE tworzone_konta (
                                   nazwa  varchar(64),
                                   haslo  varchar(64),
                                   email  varchar(256),
                                   data_wyslania_emaila  timestamp(0),
                                   kod_w_emailu  varchar(16),
                                   PRIMARY KEY ( nazwa )
);

CREATE TABLE  sesje  (
                          sid  int NOT NULL,
                          iduzytkownika  int NOT NULL,
                          ip  varchar(64),
                          poczatek  timestamp(0),
                          ost_aktywnosc  timestamp(0),
                          user_agent  varchar(1024),
                          PRIMARY KEY ( sid )
);

CREATE TABLE punkty_pogody  (
                                  idsystemu  int NOT NULL,
                                  x  double precision NOT NULL,
                                  y  double precision NOT NULL,
                                  czas timestamp(0) NOT NULL,
                                  temperatura real,
                                  opady real,
                                  predkosc_wiatru real,
                                  kierunek_wiatru real,
                                  PRIMARY KEY (x, y, czas)
);

CREATE TABLE systemy_pogodowe  (
                                idsystemu  int NOT NULL,
                                nazwa varchar(32),
                                info text
);

CREATE TABLE slady (
                                idsladu int NOT NULL,
                                idwlasciciela int,
                                nazwa varchar (256),
                                data_dodania timestamp (0),
                                punkty text,
                                widocznosc_publiczna boolean,
                                kopiowanie_dozwolone boolean,
                                PRIMARY KEY (idsladu)
);

CREATE TABLE konfiguracje (
                                idkonfiguracji int NOT NULL,
                                idsladu int,
                                idwlasciciela int,
                                data_dodania timestamp(0),
                                ost_modyfikacja timestamp(0),
                                nr_punktu_poczatkowego int,
                                data_poczatku timestamp(0),
                                srednia_predkosc_kmh real,
                                tu boolean,
                                teraz boolean,
                                widocznosc_publiczna boolean,
                                kopiowanie_dozwolone boolean,
                                typ_opcj_param int,
                                opcj_param text,
                                postoje text,
                                PRIMARY KEY (idkonfiguracji)
);

COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

FROM tomcat:latest

LABEL maintainer="Nidhi Gupta"

EXPOSE 8080

CMD ["catalina.sh", "run"]

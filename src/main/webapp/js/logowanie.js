async function zaloguj()
{
    var log = document.getElementById("tLogin").value;
    var has = document.getElementById("tHaslo").value;
    if(log.length === 0 || has.length === 0)
    {
        alert("podaj login i hasło!")
        //err
        return false
    }
    res = await fetch("/logowanie",{
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({login: log, haslo: has})
    })
    // res -> odpowiedz z serwera

    return res.json().sid
}
class Wykres {
    ctx = null
    typ = 'line'
    podpisy = []
    dane = []
    wykres = null
    constructor(mapa) {
        const footer = (tooltipItems) => {
            //
            let x = tooltipItems[0].label;
            //console.log(x)
            //console.log(danePogody[x])
            kursor.setLatLng(danePogody[x])
            return opisyPogody[x]
        };
        this.ctx = document.getElementById("wykres");
        let podpisy = this.podpisy
        let dane = this.dane
        let typ = this.typ
        this.wykres = new Chart(this.ctx, {
            type: typ,
            data: {
                labels: podpisy,
                datasets: [{
                    label: 'temperatura',
                    data: dane,

                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        //'rgba(54, 162, 235, 0.2)',
                        //'rgba(255, 206, 86, 0.2)',
                        //'rgba(75, 192, 192, 0.2)',
                        //'rgba(153, 102, 255, 0.2)',
                        //'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        //'rgba(54, 162, 235, 1)',
                        //'rgba(255, 206, 86, 1)',
                        // 'rgba(75, 192, 192, 1)',
                        //'rgba(153, 102, 255, 1)',
                        //'rgba(255, 159, 64, 1)'
                    ],

                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    tooltip: {
                        callbacks: {
                            footer: footer,
                        }
                    },
                },
            },
        })
    }
    ustawDane(podpisy, dane) {
        this.podpisy = podpisy
        this.dane = dane

        this.wykres.data.labels = podpisy
        this.wykres.data.datasets.forEach(dataset => {
            dataset.data = dane
        })

        this.wykres.update()
    }
}
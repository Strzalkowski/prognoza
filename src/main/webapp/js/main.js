let wykres;
let mymap;
let danePogody;
let opisyPogody;
let kursor;

function timeHM(ms) {
    var d = new Date(ms),
        h = (d.getHours()<10?'0':'') + d.getHours(),
        m = (d.getMinutes()<10?'0':'') + d.getMinutes();
    return  h + ':' + m;
}

const getCookieValue = (name) => (
    document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)')?.pop() || ''
)

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function main() {
    mymap = L.map('map').setView([50.00, 20.00], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoicHJvamVrdGlvIiwiYSI6ImNrdmRqMzh1djZ6N2UydXM3ejloMmV3cjQifQ.HOcupE1cuihoT8GgDKmuDg'
    }).addTo(mymap);
    console.log(mymap)

    // var latlngs = [
    //     [50, 20],
    //     [50, 21]
    // ];

    // var polyline = L.polyline(latlngs, {color: 'red'}).addTo(mymap);
    kursor = L.circle([0, 0], {
        color: '#0066ff',
        fillColor: '#00aeff',
        fillOpacity: 0.5,
        radius: 4000
    }).addTo(mymap);

// zoom the map to the polyline
   // mymap.fitBounds(polyline.getBounds());

    pobierzPunkty(mymap)
    wykres = new Wykres(mymap);
    pobierzPogode();
    ustawParametry()
}

async function pobierzPunkty(mymap) {
    sid = getCookieValue("sid")
    hasz = findGetParameter("hasz")
    res = await (await fetch("/API/slad?sid=" + sid + "&hasz=" + hasz, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        }
    })).json()
    console.log("pobierzPunkty po await")
    console.log(res)
    var latlon = []
    if (res.lat != null && res.lon != null && res.lat.length === res.lon.length) {
        for (let i = 0; i < res.lat.length; i++) {
            latlon.push([res.lat[i], res.lon[i]])
        }
    }
    var polyline = L.polyline(latlon, {color: 'blue'}).addTo(mymap);

    // zoom the map to the polyline
    mymap.fitBounds(polyline.getBounds());

}

async function pobierzPogode() {
    sid = getCookieValue("sid")
    hasz = findGetParameter("hasz")
    res = (await fetch("/API/wykres?sid=" + sid + "&hasz=" + hasz + "&startlat=" + 50.096171 + "&startlot=" + 20.071468 + "&czas_start=" + 0, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({sid: sid, hasz: hasz})
    })).json().then(data => {
        console.log(data)
        podpisy = [];
        dane = []
        danePogody = {};
        opisyPogody = {}
        data.forEach((d) => {
            czas = timeHM(d.czas_punktu* 1000)
            podpisy.push(czas)
            dane.push(((d.temp)-273.15))
            danePogody[czas] = [d.y, d.x];
            opisyPogody[czas] = d.opis
        })
        wykres.ustawDane(podpisy, dane)
    })
        /*.catch(
        () => {
            danePogody = {};
            danePogody[22] = [50, 20];
            danePogody[23] = [50, 20.5];
            danePogody[213] = [50, 21];
            wykres.ustawDane([22, 23, 213], [23, 43, 23])
        }
    )*/


}

function parametryJSON() {
    let p = document.getElementById("tPredkosc").value
    //let cz = document.getElementById("tCzasRozpoczecia").value
    let cz = document.getElementById("tCzasRozpoczecia").valueAsNumber / 1000;
    let data = document.getElementById("tDataRozpoczecia").valueAsNumber / 1000;
    return {
        predkosc: p,
        czasRozpoczecia: cz,
        dataRozpoczecia: data,
    }
}
function ustawParametry(){
    sid = getCookieValue("sid")
    hasz = findGetParameter("hasz")
    fetch("API/pobierz_parametry",{
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({sid: sid, hasz: hasz})
    }).then(response => response.json())
        .then(data => ustawParametryJSON(data));
}
function ustawParametryJSON(dane) {
    let p = document.getElementById("tPredkosc")
    let cz = document.getElementById("tCzasRozpoczecia")
    let data = document.getElementById("tDataRozpoczecia")
    p.value = dane.predkosc;
    cz.valueAsNumber = dane.czasRozpoczecia * 1000;
    data.valueAsNumber = dane.dataRozpoczecia * 1000;
}

// function zmianaTrasy() {
//     let sel = document.getElementById("sWybTrasy");
//     if (sel.value === "-") {
//         window.location.href = 'importowanie.html';
//     } else {
//         window.location.href = 'pokaz.html';
//     }
// }
function zapiszParametry() {
    fetch("/API/zapisz_parametry",{
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(parametryJSON())
    })
    pobierzPogode()
}
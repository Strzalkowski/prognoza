<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
    <link rel="stylesheet" href="css/1.css">
</head>
<body onload="main()">
<h1><%= "Hello World!" %>
</h1>
<br/>
<a href="hello-servlet">Hello Servlet</a>
<nav>
    <div>
        <a href=""></a>
    </div>
    <div>
        <a href="">Trasa</a>
    </div>
    <div>
        <a href="">Prognoza</a>
    </div>
    <div>
        <a href="">Parametry</a>
    </div>
</nav>
</body>
</html>

package pl.kinemaprognoza.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.PunktPogody;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;

//to jest opakowanie na pobieranie punktów pogody z bazy - jeśli nie znajdzie, to ściągnie z internetu
//mozliwością pozostaje dodanie do obiektu semafora i pobieranie współbieżne
public class Zasysacz {
    Baza baza = null;
    String realpath = null;
    String sid = null;
    public Zasysacz(Baza baza, String sciezka_katalogu_lokalnego, String sid)
    {
        this.baza = baza;
        this.realpath = sciezka_katalogu_lokalnego;
        this.sid = sid;
    }
    static ObjectMapper objectMapper = initObjectMapper();

    public static ObjectMapper initObjectMapper()  {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        SimpleModule module = new SimpleModule("DoubleSerializer", new Version(1, 0, 0, ""));

        module.addSerializer(Double.class, new NumberSerializers.DoubleSerializer(Double.class));
        objectMapper.registerModule(module);
        //objectMapper.registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper;
    }

    public static String getTextFromUrl(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        return response.toString();
    }

    public PunktPogody dajPunktPogody(double x, double y, long czas, Baza baza) {

        PunktPogody punkt = baza.dajPunktPogody(x,y,czas);

        x = PunktPogody.zaokrogl_x(x);
        y = PunktPogody.zaokrogl_x(y);

//        punkt.x=x;
//        punkt.y=y;
//        punkt.idsystemu = -1;

        if(punkt.idsystemu == 0)//czyli danych nie ma
        {
            System.out.println("POBIERANIE("+x+","+y+","+czas+")");

            try {
                //String content = getTextFromUrl("http://localhost/forecast.json");
                //System.out.println(realpath + "forecast.json");
                //Dostań klucz do API
                String klucz = baza.dajKlucz(sid);
                //Pobierz z sieci
                String content = "";
                if(klucz == null || "ZAŚLEPKA".equals(klucz))
                {
                    //zaślepka
                    System.out.println("ZASLEPKA");
                    content = UTIL.przeczytajPlik("/usr/local/tomcat/webapps/ROOT/forecast.json");
                }
                else{
                    try {
                        System.out.println( "UTIL.getTextFromUrl(\"http://api.openweathermap.org/data/2.5/forecast?lat=" + x + "&lon=" + y + "&appid=" + klucz+");");
                         //content = UTIL.przeczytajPlik("/usr/local/tomcat/webapps/ROOT/forecast.json");
                         content = UTIL.getTextFromUrl("http://api.openweathermap.org/data/2.5/forecast?lat=" + x + "&lon=" + y + "&appid=" + klucz);
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                        PunktPogody p = new PunktPogody();
                        p.idsystemu =0;
                        p.x=x;
                        p.y=y;
                        p.czas_punktu = czas;
                        p.czas_generacji =czas;
                        p.opis = "BŁĄD POBIERANIA";
                        return p;
                    }
                }

                //String content = UTIL.przeczytajPlik("/usr/local/tomcat/webapps/ROOT/forecast.json");


                //System.out.print(content+"\n\n");
                JsonNode root = objectMapper.readTree(content);
                if (!root.has("cod") || !root.has("cnt")) {
                    System.err.println("JSON zepsuty:OWM API");
                    return new PunktPogody();
                }
                int cod = root.get("cod").asInt(401);
                int cnt = root.get("cnt").asInt(0);
                String miejscowosc  = root.get("city").get("name").asText("<nieznana miejscowość>");
                System.out.println("cnt:"+cnt);
                if (cod != 200) {
                    //baza .... itd - pobierz z bazy
                    return new PunktPogody();
                }
                JsonNode list = root.get("list");
                for (int i = 0; i < cnt; i++)
                {
                    System.out.println("-------" + i + "----------");
                    JsonNode jpt = list.get(i);
                    PunktPogody pkt = new PunktPogody();
                    pkt.x = x;
                    pkt.y = y;
                    pkt.idsystemu = 1;
                    pkt.czas_punktu = jpt.get("dt").asLong(czas);
                    pkt.czas_generacji = System.currentTimeMillis() / 1000L;;
                    JsonNode main = jpt.get("main");
                    JsonNode weather = jpt.get("weather");
                    JsonNode wind = jpt.get("wind");

                    pkt.temp = main.get("temp").asDouble(0);
                    pkt.temp_odczuwalna = main.get("feels_like").asDouble(0);
                    pkt.cisnienie_zreduk = main.get("sea_level").asDouble(0);
                    pkt.cisnienie_rzeczyw = main.get("grnd_level").asDouble(0);
                    pkt.wilgotnosc = main.get("humidity").asDouble(0);

                    pkt.opis =  weather.get(0).get("description").asText("<brak opisu>");// +", " +Instant.ofEpochSecond(pkt.czas_punktu).toString();

                    pkt.predkosc_wiatru = wind.get("speed").asDouble(0);
                    pkt.kierunek_wiatru = wind.get("deg").asDouble(360.0);

                    objectMapper.writeValue(System.out, pkt);
                    baza.dodajPunktPogody(pkt);
                    System.out.println("########ABA##############");
                    //objectMapper.writeValue(System.out, pt);
                    //objectMapper.writeValue(System.out,list.get(i));
                    //System.out.println(list.get(i).asText());
                }

                System.out.print("Zasysacz:");
                objectMapper.writeValue(System.out, punkt);
                return baza.dajPunktPogody(x,y,czas);
            } catch (Throwable e) {
                System.out.println("\nWYPADEK PRZY PARSOWANIU JSONA Z OPENWEATHERMAP");
                e.printStackTrace();
                return new PunktPogody();//coś trzeba zwrócić
            }
        }

        return punkt;
    }
}

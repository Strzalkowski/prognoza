package pl.kinemaprognoza.util;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.stream.Collectors;

import static java.lang.Math.*;

//to jest miejsce z różnymi pomocniczymi funkcjami, które nie mają oczywistego mijesca w hierarchii klas.
public class UTIL {
    private static Random r = new Random();
    public static String losowy_String_z_malymi_literami(int dlugosc)
    {
        StringBuilder sb = new StringBuilder(dlugosc);
        char c;
        for(int i=0; i<dlugosc;i++)
        {
            c = (char)(r.nextInt(26) + 'a');
            sb.append(c);
        }
        return sb.toString();
    }

    //Zaokrągla double do podanej liczby miejsc po przecinku. Ujemne miejsca traktuje jak 0.
    //Jak mu sie cokolwiek nie spodoba, zwraca Double.NaN
    public static double zaokrągl(double liczba, int miejsca) {
        try{
            if (miejsca < 0) {miejsca =0;}

            BigDecimal bd = new BigDecimal(Double.toString(liczba));
            bd = bd.setScale(miejsca, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }
        catch(Throwable t)
        {
            return Double.NaN;
        }
    }

    public static long epochPoprzedniaPelnaGodzina(long e)
    {
        return (e/3600)*3600;
    }
    public static long epochNajblizszaPelnaGodzina(long e)
    {
        long d = (e/3600)*3600;
        return ((e%3600>1799)?(d+3600):(d));
    }


    private static Double toRad(Double value) {
        return value * Math.PI / 180;
    }
    /**haversine(a) = sin^2(a/2)*/
    private static double  hav(double k/*[rad]*/)
    {
        double s = sin(k/2);
        return s*s;
    }


    /**
     * Oblicza odległość pomiędzy środkami wyznaczonymi przez dwa punkty o podanych współrzędnych geograficznych (w stopniach)
     * @param x1 pierwsza długość geograficzna (w stopniach)
     * @param y1 pierwsza szerokość geograficzna (w stopniach)
     * @param x2 druga długość geograficzna (w stopniach)
     * @param y2 druga szerokość geograficzna (w stopniach)
     * @return odległość w kilometrach
     */

    public static double odległośćSferyczna(double x1/*[deg]*/, double y1/*[deg]*/, double x2/*[deg]*/, double y2/*[deg]*/){

        x1 = toRad(x1);
        x2 = toRad(x2);

        y1 = toRad(y1);
        y2 = toRad(y2);
        Double R = 6371.008;//[km] - średni promień Ziemi

        return 2*R*asin( sqrt( hav(x2-x1) + (1 - hav(x1-x2) - hav(x1+x2)) * hav(y2-y1) ) );

    }


    public static String przeczytajPlik(String filePath)
    {
        String content = "";

        try
        {
            content = new String ( Files.readAllBytes( Paths.get(filePath) ) );
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return content;
    }

    public static String getTextFromUrl(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        return response.toString();
    }

    static ObjectMapper objectMapper = initObjectMapper();

    public static ObjectMapper initObjectMapper()  {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("DoubleSerializer", new Version(1, 0, 0, ""));

        module.addSerializer(Double.class, new NumberSerializers.DoubleSerializer(Double.class));
        objectMapper.registerModule(module);
        //objectMapper.registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper;
    }

    public static Uzytkownik autoryzacja(HttpServletRequest request, HttpServletResponse response, Baza baza)
    {
        return autoryzacja(null, request, response, baza);
    }
    public static Uzytkownik autoryzacja(String sid_opcj, HttpServletRequest request, HttpServletResponse response, Baza baza)
    {
        String sid;
        if(sid_opcj == null) {
            //znajdż identyfikator sesji - sid, albo cookie
            sid = request.getParameter("sid");
            if (sid == null) {
                Cookie[] cookies = request.getCookies();
                if (cookies != null) {
                    for (Cookie ciasteczko : cookies) {
                        if (ciasteczko.getName().equals("sid")) {
                            sid = ciasteczko.getValue();
                            System.out.println("1sid:"+sid);
                        }
                    }
                }
            }

            try {
                if (sid == null && "POST".equalsIgnoreCase(request.getMethod()))//&& request.getContentType() != null && request.getContentType() == "application/json")
                {
                    //System.out.println("JSON!!!::::\n");
                    String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
                    //System.out.println(body);
                    JsonNode root = objectMapper.readTree(body);
                    sid = root.get("sid").asText("");
                    System.out.println("2sid:"+sid);
                }
            } catch (Throwable t) {
                t.printStackTrace();
                System.out.println("BLAD PARSOWANIA JSONA PRZYCHODZACEGO");
            }
        }
        else{sid=sid_opcj;}

        Uzytkownik uzytkownik = baza.dajUzytkownika(sid);
        if(uzytkownik == null)
        {
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter out = null;
            try {
                out = response.getWriter();
                out.write("<h1>FIGĘ!</h1><br>AUTORYZACJA SIĘ NIE POWIODŁA<br>403:Zabronione<br>");
                out.write("sid:"+sid);
                Cookie ck[]=request.getCookies();
                for (Cookie ciasteczko: ck) {
                    out.print("<br>" + ciasteczko.getName() + "|" + ciasteczko.getValue());//printing name and value of cookie
                    if(ciasteczko.getName().equals("sid")){sid = ciasteczko.getValue(); out.println("ROWNASIE"); out.println(sid);}
                }
                response.setStatus(403);//Forbidden
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
        else{return uzytkownik;}
    }


    public static boolean autoryzacja_do_hasza(String sid_opcj, String hasz_opcj, HttpServletRequest request, HttpServletResponse response, Baza baza) {
        String sid;
        String hasz;
        if(sid_opcj == null)
        {
            //znajdż identyfikator sesji - sid, albo cookie
            sid = request.getParameter("sid");
            if (sid == null) {
                Cookie[] cookies = request.getCookies();
                for (Cookie ciasteczko : cookies) {
                    if (ciasteczko.getName().equals("sid")) {
                        sid = ciasteczko.getValue();
                    }
                }
            }
        }
        else {sid = sid_opcj;}
        if(hasz_opcj == null)
        {
            hasz = request.getParameter("hasz");

            try {
                if ( (hasz == null || sid == null) && "POST".equalsIgnoreCase(request.getMethod()) )//&& request.getContentType() != null && request.getContentType() == "application/json")
                {
                    //System.out.println("JSON!!!::::\n");
                    String body  = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
                    //System.out.println(body);
                    JsonNode root = objectMapper.readTree(body);
                    sid = root.get("sid").asText("");
                    hasz = root.get("hasz").asText("");
                }
            }
            catch(Throwable t){t.printStackTrace(); System.out.println("BLAD PARSOWANIA JSONA PRZYCHODZACEGO");}
        }
        else {hasz=hasz_opcj;}



        boolean wpuszczony = baza.prawo_widoku_konfiguracji(hasz, sid);

        try {
            PrintWriter out = response.getWriter();
            if (!wpuszczony)
            {
                if( request.getContentType() != null && request.getContentType().equals("application/json"))
                {
                    out.write("{\"message\": \"403:Forbidden\"}");
                    response.setStatus(403);
                    return false;
                }
                else{
                    response.setContentType("text/html; charset=UTF-8");
                    out.write("<h1>ODMOWA DOSTĘPU DO PROGNOZY</h1><br>");
                    out.write("Prognoza pod tym adresem nie jest twoja, lub nie jest ustawiona jako publiczna<br>");
                    out.write("A poza tym jestem czajnikiem!(418)<br>");
                    response.setStatus(418);//I am a teapot!
                    return false;
                }
            }
            else{return true;}


        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static boolean powiadomienie_o_bledzie(HttpServletRequest request, HttpServletResponse response, String komunikat) {

        try {

            PrintWriter out = response.getWriter();
            if( request.getContentType() != null && request.getContentType().equals("application/json"))
            {
                out.write("{\"message\": \""+komunikat+"\"}");
                response.setStatus(500);
                return false;
            }
            else{
                response.setContentType("text/html; charset=UTF-8");
                out.write("<h1>500:Wewnętrzny błąd aplikacji</h1><br>");
                out.write(komunikat+"<br>");
                response.setStatus(500);//I am a teapot!
                return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }
}
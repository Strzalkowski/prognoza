package pl.kinemaprognoza.orm;

import pl.kinemaprognoza.orm.rekordy.Konfiguracja;
import pl.kinemaprognoza.orm.rekordy.PunktPogody;
import pl.kinemaprognoza.orm.rekordy.Sesja;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;
import pl.kinemaprognoza.util.UTIL;

import java.sql.*;
import java.sql.CallableStatement;
import java.util.ArrayList;



public class Baza {
    static String loginUser = "admin";
    static String loginPasswd = "Nebuzaradan!";
    static String loginUrl = "jdbc:postgresql://postgres_container:5432/prognozy";
    private Connection conn = null;

    SQLException ostatnibłąd;
    public SQLException dajOstatniBłąd(){return ostatnibłąd;}

    public Baza()
    {
        połącz();
    }

    /**
     * Łączy się z bazą danych
     * @return true, jeśli dostał połączenie, false jeśli jakiś błąd SQL, wyrzuca RuntimeException jeśli nie mógł załadować JDBCDrivera
     */
    boolean połącz() {
        // Load the PostgreSQL driver
        try {
            Class.forName("org.postgresql.Driver");
            if(conn != null){rozłącz();}//żeby nie dało sie otworzyć więcej jak jednego połączenia na obiekt
            conn = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
        } catch (ClassNotFoundException ex) {
            System.err.println("ClassNotFoundException: " + ex.getMessage());
            throw new RuntimeException("Class not found Error");
        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Jeśli dbcon nie jest nullem zamyka połączenie
     */
    public void rozłącz()
    {
        if(conn == null){return;}
        try {
            conn.close();
            conn =null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean dodajUzytkownika(Uzytkownik u)
    {
        final String sql = "INSERT INTO uzytkownicy (nazwa, haslo, email) VALUES (?, ?, ?);";
        try {ostatnibłąd=null;

            PreparedStatement rozkaz = conn.prepareStatement(sql);
            rozkaz.setString(1,u.nazwa);
            rozkaz.setString(2,u.haslo);
            rozkaz.setString(3,u.email);
            rozkaz.executeUpdate();

        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ArrayList<Uzytkownik> dajListęUzytkownikow()
    {
        ArrayList<Uzytkownik> uzytkownicy = new ArrayList<Uzytkownik>();
        String zapytanie = "SELECT iduzytkownika, nazwa, email FROM uzytkownicy;";

        try (Statement stmt = conn.createStatement()) {
            ostatnibłąd=null;
            ResultSet odp = stmt.executeQuery(zapytanie);
            ResultSetMetaData meta = odp.getMetaData();
            System.err.printf("długość dł:%d\n", meta.getColumnCount());
            while (odp.next()) {
                System.err.printf("DUPA \n");
                Uzytkownik uz = new Uzytkownik();
                uz.nazwa = odp.getString("nazwa");
                uz.email = odp.getString("email");
                uz.haslo = null;
                uz.id = odp.getInt("iduzytkownika");
                uzytkownicy.add(uz);
            }
        } catch (SQLException e) {
            ostatnibłąd = e;
           e.printStackTrace(System.err);
        }
        return uzytkownicy;
    }

    protected void finalize()
    {
        //upewnij się, że połaczenie zostało zamknięte
        rozłącz();
    }
    /**
     * @return zwraca ubiekt typu Uzytkownik, albo null, jaśli nie ma takiego, albo pojawił sie jakowyś błąd
    */

    public Uzytkownik dajUzytkownika(String nazwa, String haslo)
    {
        final String sql = "SELECT iduzytkownika, email FROM uzytkownicy WHERE nazwa=? AND haslo=? LIMIT 1;";
        Uzytkownik uz = null;
        try {ostatnibłąd=null;

            PreparedStatement kwerenda = conn.prepareStatement(sql);
            kwerenda.setString(1,nazwa);
            kwerenda.setString(2,haslo);
            ResultSet odp =  kwerenda.executeQuery();
            if(odp.next())
            {
                uz = new Uzytkownik();
                uz.id = odp.getInt("iduzytkownika");
                uz.nazwa = nazwa;
                uz.haslo = haslo;
                uz.email = odp.getString("email");
            }
            return uz;

        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace();
            return null;
        }

    }
    public Uzytkownik/*Bez hasła*/ dajUzytkownika(String sid)
    {
        final String sql = "SELECT iduzytkownika, email,nazwa FROM uzytkownicy" +
                " JOIN sesje USING(iduzytkownika)" +
                " WHERE sid=? LIMIT 1;";
        Uzytkownik uz = null;
        try {ostatnibłąd=null;

            PreparedStatement kwerenda = conn.prepareStatement(sql);
            System.out.println("AUTORYZACJA DLA sid:"+sid);
            kwerenda.setString(1,sid);
            System.out.println("AUTORYZACJA DLA sid:"+sid);
            ResultSet odp =  kwerenda.executeQuery();
            if(odp.next())
            {
                uz = new Uzytkownik();
                uz.id = odp.getInt("iduzytkownika");
                uz.nazwa = odp.getString("nazwa");
                uz.haslo = null;
                uz.email = odp.getString("email");
            }
            System.out.println("Zwraca"+uz);
            return uz;

        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace();
            return null;
        }
    }
    public Sesja dajSesje(String sid)
    {
        final String sql = "SELECT iduzytkownika FROM sesje WHERE sid=? LIMIT 1;";
        Sesja s = null;
        try {ostatnibłąd=null;

            PreparedStatement kwerenda = conn.prepareStatement(sql);
            kwerenda.setString(1,sid);
            ResultSet odp =  kwerenda.executeQuery();
            if(odp.next())
            {
                s = new Sesja();
                s.iduzutkownika = odp.getInt("iduzytkownika");
            }
            return s;

        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace();
            return null;
        }
    }

    public Sesja nowaSesja(Uzytkownik u)
    {
        final String sql = "INSERT INTO sesje (sid, iduzytkownika, poczatek) VALUES (?, ?, CURRENT_TIMESTAMP);";
        try {ostatnibłąd=null;

            System.out.println("NOW SESJA!");
            String sid;
            do{
                sid = UTIL.losowy_String_z_malymi_literami(32);
            }while(dajSesje(sid)!=null);//nie chcemy wstawiać duplikatów (ani bawić się z błędami bazy)

            PreparedStatement rozkaz = conn.prepareStatement(sql);
            rozkaz.setString(1, sid);
            rozkaz.setInt(2, u.id);
            rozkaz.executeUpdate();
            System.out.println("NOWA SESJA sid="+sid+"iduzytkownika="+u.id);
            return new Sesja(sid, u.id);

        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace();
            return null;
        }
    }

    //Po prostu jakiś identyfikator konfiguracji będzie wystawiony na świat w adresach,
    //więc lepiej, żeby to nie był dokładnie ten, co w bazie
    private int hasz_do_idkonfiguracji(String hasz)
    {
        int i=0;
        try{i = Integer.parseInt(hasz);}
        catch(Throwable t) {
            i=0;//w bazie danych nie będzie rekordu o kluczu 0
        }
        return golden32rev(i);
    }
    private String idkonfiguracji_do_hasz(int idkonfiguracji)
    {
        return Integer.toString(golden32fwd(idkonfiguracji));
    }
    //jakaś najprostsza implementacja, dobrze by było później zmienić
    static int GMULI = 0x9E3779B9;
    static int GDIVI = 0x144CBC89;
    static int GADDI = 0x01234567;
    public static int golden32fwd(int key) {
        key *= GMULI;
        key += GADDI;
        return key;
    }

    public static int golden32rev(int key) {
        key -= GADDI;
        key *= GDIVI;
        return key;
    }
    //a tutaj wersja 64bit, jakby kiedyś była potrzebna
    static long GMULL = 0x9E3779B97F4A7C15L;
    static long GDIVL = 0xF1DE83E19937733DL;
    static long GADDL = 0x0123456789ABCDEFL;
    public static long golden64fwd(long key) {
        key *= GMULL;
        key += GADDL;
        return key;
    }

    public static long golden64rev(long key) {
        key -= GADDL;
        key *= GDIVL;
        return key;
    }

    public void dodajKonfiguracje(String json , String nazwa , Uzytkownik wlasciciel, boolean podmienic)
    {
        try {ostatnibłąd=null;
            System.out.print(json+"\n"+nazwa+"\n"+wlasciciel+"\n"+podmienic);
            CallableStatement cStmt = conn.prepareCall("CALL dodajKonfiguracje(?, ?, ?, ?);");
            //jeśli by trzeba było oodzyskać parametry wyjściowe (których tu akurat nie ma),
            //to trzeba je wcześniej zarejestrować
            //cStmt.registerOutParameter(2, Types.INTEGER);
            cStmt.setString(1,json);
            cStmt.setString(2,nazwa);
            cStmt.setInt(3,wlasciciel.id);
            cStmt.setBoolean(4,podmienic);
            System.out.println("dodajKonfiguracje");
            System.out.print(json+"\n"+nazwa+"\n"+wlasciciel+"\n"+podmienic);
            cStmt.execute();
            System.out.println("przeżyłem egzekucję");

        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace();
        }
    }

    public String dajPostoje_Json(String hasz)
    {
        int idkonfiguracji = hasz_do_idkonfiguracji(hasz);
        String postoje = null;
        String zapytanie = "SELECT postoje FROM konfiguracje WHERE idkonfiguracji = ? LIMIT 1;";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setInt(1, idkonfiguracji);
            ResultSet odp = stmt.executeQuery();
            if (odp.next()) {
                postoje = odp.getString(1);
            }
        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
        }
        return postoje;
    }

    public String dajPunktySladu(String hasz)
    {
        int idkonfiguracji = hasz_do_idkonfiguracji(hasz);
        String json = null;
        String zapytanie = "SELECT punkty FROM konfiguracje LEFT JOIN slady USING(idsladu) WHERE idkonfiguracji=? LIMIT 1";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setInt(1, idkonfiguracji);
            ResultSet odp = stmt.executeQuery();
            if (odp.next()) {
                json = odp.getString(1);
            }
        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
        }
        return json;
    }

    public boolean ustawPostoje(String hasz, String postoje_json)
    {
        int idkonfiguracji = hasz_do_idkonfiguracji(hasz);
        String zapytanie = "UPDATE konfiguracje SET postoje = ? WHERE idkonfiguracji = ?;";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setString(1, postoje_json);
            stmt.setInt(2, idkonfiguracji);
            stmt.executeUpdate();

        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
            return false;
        }
        return true;
    }

    public String dajParametry_Json(String hasz)
    {
        int idkonfiguracji = hasz_do_idkonfiguracji(hasz);
        String param = null;
        String zapytanie = "SELECT opcj_param FROM konfiguracje WHERE idkonfiguracji = ? LIMIT 1;";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setInt(1, idkonfiguracji);
            ResultSet odp = stmt.executeQuery();
            if (odp.next()) {
                param = odp.getString(1);
            }
        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
        }
        return param;
    }
    public boolean ustawParametry(String hasz, String opcj_param)
    {
        int idkonfiguracji = hasz_do_idkonfiguracji(hasz);
        String zapytanie = "UPDATE konfiguracje SET opcj_param = ? WHERE idkonfiguracji = ?;";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setString(1, opcj_param);
            stmt.setInt(2, idkonfiguracji);
            stmt.executeUpdate();

        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
            return false;
        }
        return true;
    }



    public ArrayList<Konfiguracja> dajListeKonfiguracji(int iduzytkownika)
    {
        ArrayList<Konfiguracja> konfiguracje = new ArrayList<Konfiguracja>();
        String zapytanie = "SELECT idkonfiguracji, nazwakonfiguracji, opcj_param, postoje, idsladu FROM konfiguracje WHERE idwlasciciela = ?;";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setInt(1, iduzytkownika);
            ResultSet odp = stmt.executeQuery();
            ResultSetMetaData meta = odp.getMetaData();
            System.err.printf("długość dł:%d\n", meta.getColumnCount());
            while (odp.next()) {
                Konfiguracja k = new Konfiguracja();
                k.hasz = idkonfiguracji_do_hasz( odp.getInt("idkonfiguracji") );
                k.idsladu = odp.getInt("idsladu");
                k.nazwakonfiguracji = odp.getString("nazwakonfiguracji");
                k.opcj_param = odp.getString("opcj_param");
                k.postoje = odp.getString("postoje");
                konfiguracje.add(k);

            }
        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
        }
        return konfiguracje;
    }
    public boolean prawo_widoku_konfiguracji(String hasz, String sid )
    {
        if( sid == null || sid.length() > 33 ){return false;}
        int idkonfiguracji = hasz_do_idkonfiguracji(hasz);
        boolean zwr = false;
        String zapytanie = "SELECT prawo_widoku_konfiguracji(?, ?);\n;";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setInt(1, idkonfiguracji);
            stmt.setString(2,sid);
            ResultSet odp = stmt.executeQuery();
            if (odp.next()) {
                zwr = odp.getBoolean(1);
            }
        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
        }
        return zwr;
    }

    public PunktPogody dajPunktPogody(double x, double y, long czas) {

        PunktPogody pkt = new PunktPogody();
        x = PunktPogody.zaokrogl_x(x);
        y = PunktPogody.zaokrogl_x(y);
        Timestamp t = new Timestamp(czas*1000);
        String zapytanie = "SELECT abs(czas_punktu - ?) AS odl, idsystemu, x,y, czas_punktu, czas_generacji, temperatura, temperatura_odczuwalna, cisnienie_zreduk, cisnienie_rzeczyw, wilgotnosc, opady, predkosc_wiatru, kierunek_wiatru, opis FROM punkty_pogody"
                + " WHERE x = ? AND y = ? AND abs(czas_punktu - ?) < '2 hours'::interval AND abs(CURRENT_TIMESTAMP - czas_generacji) < '4 hours'::interval ORDER BY odl ASC LIMIT 1";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setTimestamp(1, t);
            stmt.setTimestamp(4, t);
            stmt.setDouble(2,x);
            stmt.setDouble(3,y);
            ResultSet odp = stmt.executeQuery();
            if (odp.next()) {
                pkt.idsystemu = odp.getInt("idsystemu");
                pkt.x = odp.getDouble("x");
                pkt.y = odp.getDouble("y");
                pkt.czas_punktu = odp.getTimestamp("czas_punktu").getTime()/1000;
                pkt.czas_generacji = odp.getTimestamp("czas_generacji").getTime()/1000;
                pkt.temp = odp.getDouble("temperatura");
                pkt.temp_odczuwalna = odp.getDouble("temperatura_odczuwalna");
                pkt.cisnienie_zreduk = odp.getDouble("cisnienie_zreduk");
                pkt.cisnienie_rzeczyw = odp.getDouble("cisnienie_rzeczyw");
                pkt.opis = odp.getString("opis");
                pkt.wilgotnosc = odp.getDouble("wilgotnosc");
                pkt.predkosc_wiatru = odp.getDouble("predkosc_wiatru");
                pkt.kierunek_wiatru = odp.getDouble("kierunek_wiatru");
            }
        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
        }
        return pkt;
    }
    public void dodajPunktPogody(PunktPogody p)
    {
        final String sql = "INSERT INTO punkty_pogody (idsystemu, x,y, czas_punktu, czas_generacji, temperatura, temperatura_odczuwalna, cisnienie_zreduk, cisnienie_rzeczyw, wilgotnosc, opady, predkosc_wiatru, kierunek_wiatru, opis)" +
                                    " VALUES (?,          ?,?   ,?,             ?,              ?,              ?,                  ?,                  ?,                  ?,       ?,     ?,                  ?,            ?);";
        //                                    1,          2,3   ,4,             5,              6,              7,                  8,                  9,                  10,       11,     12,                13,            14);";
        try {ostatnibłąd=null;

            PreparedStatement r = conn.prepareStatement(sql);
            r.setInt(1,p.idsystemu);
            r.setDouble(2,p.x);
            r.setDouble(3,p.y);
            System.out.println("czas punktu:"+p.czas_punktu+", "+(new Timestamp(p.czas_punktu).toString()));
            r.setTimestamp(4,new Timestamp(p.czas_punktu*1000));
            r.setTimestamp(5,new Timestamp(p.czas_generacji*1000));
            r.setDouble(6,p.temp);
            r.setDouble(7,p.temp_odczuwalna);
            r.setDouble(8,p.cisnienie_zreduk);
            r.setDouble(9,p.cisnienie_rzeczyw);
            r.setDouble(10,p.wilgotnosc);
            r.setDouble(11,p.opady);
            r.setDouble(12,p.predkosc_wiatru);
            r.setDouble(13,p.kierunek_wiatru);
            r.setString(14,p.opis);
            r.executeUpdate();

        } catch (SQLException e) {
            ostatnibłąd = e;
            //tutaj dość często będzie dochodzić do wyjątków: duplicate key
            //e.printStackTrace();
        }
    }

    public String dajKlucz(String sid) {
        if(sid == null || sid.length() > 33 ){return null;}
        String zwr = null;
        String zapytanie = "select DAJ_KLUCZ_DO_OWM( ? );";

        try  {ostatnibłąd=null;
            PreparedStatement stmt = conn.prepareStatement(zapytanie);
            stmt.setString(1,sid);
            ResultSet odp = stmt.executeQuery();
            if (odp.next()) {
                zwr = odp.getString(1);
            }
        } catch (SQLException e) {
            ostatnibłąd = e;
            e.printStackTrace(System.err);
        }
        return zwr;
    }
}

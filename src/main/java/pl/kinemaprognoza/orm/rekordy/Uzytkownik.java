package pl.kinemaprognoza.orm.rekordy;
/*
CREATE TABLE uzytkownicy (
                                iduzytkownika SERIAL,
                                nazwa  varchar(64),
                                haslo  varchar(64),
                                email  varchar(256),
                                data_utworzenia  timestamp(0) DEFAULT CURRENT_TIMESTAMP,
                                ost_aktywnosc  timestamp(0),
                                PRIMARY KEY ( iduzytkownika )
);
}
 */
public class Uzytkownik {
    public Uzytkownik(){}
    public Uzytkownik(String nazwa, String haslo, String email){this.nazwa = nazwa; this.haslo=haslo; this.email=email;}
    public int id = -1;
    public String nazwa;
    public String haslo;
    public String email;
}

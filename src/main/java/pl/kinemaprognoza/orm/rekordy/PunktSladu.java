package pl.kinemaprognoza.orm.rekordy;

public class PunktSladu {
    public double lat;
    public double lon;
    public double elev;

    public PunktSladu(double lat, double lon, double elev) {
        this.lat = lat;
        this.lon = lon;
        this.elev = elev;
    }

    public PunktSladu() {}
}

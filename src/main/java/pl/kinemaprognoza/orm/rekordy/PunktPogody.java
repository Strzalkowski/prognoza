package pl.kinemaprognoza.orm.rekordy;

import pl.kinemaprognoza.util.UTIL;

public class PunktPogody {
    public int idsystemu;// 0 -nie ma takiego punktu
    public double x;//longitude - długość geogr.
    public double y; //latitude - szerokość geogr.
    public double temp;
    public double temp_odczuwalna;
    public double cisnienie_zreduk;
    public double cisnienie_rzeczyw;
    public double wilgotnosc;
    public String opis;
    public double predkosc_wiatru;
    public double kierunek_wiatru;
    public long czas_punktu;//UNIX EPOCH
    public long czas_generacji;//UNIX EPOCH
    public double opady;

    //to nie jest do końca dobre rozwiązanie
    public static double zaokrogl_x(double x)
    {
        return UTIL.zaokrągl(x,1);
    }
    public static double zaokrogl_y(double y)
    {
        return UTIL.zaokrągl(y,1);
    }
}

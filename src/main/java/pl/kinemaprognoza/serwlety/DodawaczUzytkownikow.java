package pl.kinemaprognoza.serwlety;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;

import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;

public class DodawaczUzytkownikow extends HttpServlet {

    private void piszbłąd(PrintWriter wy, String kominukat)
    {
        wy.print("WYSTĄPIŁ BŁĄD PRZY DODAWANIU UŻYTKOWNIKA<br>");
        wy.print(kominukat+ "<br>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String nazwa = request.getParameter("nazwa");
        String haslo = request.getParameter("haslo");
        String email = request.getParameter("email");

        response.setContentType("text/html");
        PrintWriter wy = response.getWriter();

        if(nazwa == null || haslo == null || email == null){piszbłąd(wy,"nazwa/haslo/emial jest puste");}
        else {
            wy.print("LISTING<br>");

            //połącznie z bazą:
            Baza baza = new Baza();
            boolean p = baza.dodajUzytkownika(new Uzytkownik(nazwa,haslo,email));
            ArrayList<Uzytkownik> uzytkownicy = baza.dajListęUzytkownikow();
            if(!p){piszbłąd(wy, "Nie powiodło się dodawanie nowego użytkownika");}
            baza.rozłącz();
            //przekieruj na listę użytkowników
            response.sendRedirect(request.getContextPath()+"/logowanie.html");

        }

    }

}

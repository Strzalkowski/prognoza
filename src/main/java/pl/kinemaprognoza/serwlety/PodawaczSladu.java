package pl.kinemaprognoza.serwlety;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.Konfiguracja;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;
import pl.kinemaprognoza.util.UTIL;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class PodawaczSladu extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //argumenty
        String sid = request.getParameter("sid");
        String hasz = request.getParameter("hasz");

        //inicjalizacja i autoryzacja
        Baza baza = new Baza();
        Uzytkownik uzytkownik =  UTIL.autoryzacja(request, response,baza);
        if(uzytkownik == null){return;/*autoryzacja juz jakis tam komunikat dopisała*/}
        if(!UTIL.autoryzacja_do_hasza(null, null,request, response, baza)){return;}

        //
        //response.setContentType("text/html");
        //response.setContentType("application/json");
        PrintWriter wy = response.getWriter();
        //wy.print("<h1>PUNKTY:</h1><br>");
        //wy.print("nazwa użytkownika:"+uzytkownik.nazwa+"<br>");
        String json = baza.dajPunktySladu(hasz);
        if(baza.dajOstatniBłąd() != null){
            response.setContentType("text/html");
            response.setStatus(500);
            baza.dajOstatniBłąd().printStackTrace(wy);
            wy.print("<p>JSON:");
            wy.print(json);
        }

        response.setContentType("application/json");
        wy.print(json);

        baza.rozłącz();
    }

}
package pl.kinemaprognoza.serwlety;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import io.jenetics.jpx.GPX;
import io.jenetics.jpx.Track;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import pl.kinemaprognoza.serwlety.typyInterfejsowe.PunktySladu;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

//@WebServlet(name = "Importer", value = "/Importer")
//@WebServlet(name = "FileUploadServlet", urlPatterns = { "/fileuploadservlet" })
//https://docs.oracle.com/javaee/6/tutorial/doc/glraq.html
//https://stackoverflow.com/questions/34887382/whats-the-difference-between-doget-dopost-and-processrequest
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class Importer_vebose extends HttpServlet {
    ObjectMapper objectMapper = null;

    @Override
    public void init() throws ServletException {
        super.init();
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("DoubleSerializer", new Version(1, 0, 0, ""));

        module.addSerializer(Double.class, new NumberSerializers.DoubleSerializer(Double.class));
        objectMapper.registerModule(module);
        //objectMapper.registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /* Receive file uploaded to the Servlet from the HTML5 form */
        /*Part filePart = request.getPart("file");
        String fileName = filePart.getSubmittedFileName();
        for (Part part : request.getParts()) {
            part.getInputStream();
            //part.write("C:\\upload\\" + fileName);
        }
        */
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String nazwap_uzytk = request.getParameter("nazwap"); // Retrieves <input type="text" name="nazwap">
        out.print("NAZWA OD UZYTKOWNIKA:"+nazwap_uzytk+"<br>\n");
        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">

        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        out.print("NAZWA PLIKU:"+fileName+"<br>\n");

        //InputStream zawartoscPliku = filePart.getInputStream();


        /*StringBuilder zawartosc = new StringBuilder();
        byte[] buf = new byte[8192];
        int length;
        while ((length = zawartoscPliku.read(buf)) > 0) {
            //out.write(String.valueOf(buf), 0, length);
            //out.print(Arrays.toString(buf));
            //sb.append(buf);
            zawartosc.append(new String(buf, StandardCharsets.UTF_8));
        }
        out.print(zawartosc.toString());
        out.print("<br>KONIEC3<br><p>");
        */

        /*
        final GPX gpx = GPX.builder()
                .addTrack(track -> track
                        .addSegment(segment -> segment
                                .addPoint(p -> p.lat(48.2081743).lon(16.3738189).ele(160))
                                .addPoint(p -> p.lat(48.2081743).lon(16.3738189).ele(161))
                                .addPoint(p -> p.lat(48.2081743).lon(16.3738189).ele(162))))
                .build();
        out.print( GPX.writer().toString(gpx) );
        */
        //czytanie GPX
        InputStream zawartoscPliku = filePart.getInputStream();

        /*GPX.read(zawartoscPliku).tracks()
                .flatMap(Track::segments)
                .flatMap(TrackSegment::points)
                .forEach(out::println);
*/
        List<Track> tracks = GPX.read(zawartoscPliku).getTracks();
        int tracknum=0;
        for(Track track : tracks)
        {
            Optional<String> trackname_optional_wrapper_for_gods_know_what = track.getName();
            String trackname = null;
            if(trackname_optional_wrapper_for_gods_know_what.isPresent()){trackname=trackname_optional_wrapper_for_gods_know_what.get();}

            int segnum=0;
            List<TrackSegment> segments = track.getSegments();

            for(TrackSegment segment : segments)
            {
                List<WayPoint> points = segment.getPoints();
                String nazwa_sladu;
                if(tracks.size()==1 && segments.size()==1){nazwa_sladu = nazwap_uzytk;}
                else
                {
                    nazwa_sladu = nazwap_uzytk+"_"+((trackname == null)?(trackname):(""+tracknum))+"_"+segnum;
                }
                PunktySladu punkty_sladu = new PunktySladu();
                punkty_sladu.lat = new double[points.size()];
                punkty_sladu.lon = new double[points.size()];
                punkty_sladu.elev = new double[points.size()];

                int i=0;
                for(WayPoint point :points)
                {
                    //PunktSladu p = new PunktSladu();
                    //p.lat = point.getLatitude().doubleValue();
                    //p.lon = point.getLongitude().doubleValue();
                    //p.elev= ((point.getElevation().isPresent())?(point.getElevation().get().doubleValue()):(null));
                    //punkty_sladu.add(p);
                    punkty_sladu.lat[i] = point.getLatitude().doubleValue();
                    punkty_sladu.lon[i] = point.getLongitude().doubleValue();
                    punkty_sladu.elev[i] = ((point.getElevation().isPresent())?(point.getElevation().get().doubleValue()):(Double.NaN));
                    i+=1;
                }

                out.println("<p>NAZWA SLADU:" +nazwa_sladu+" punkty:<br>");
                System.out.println("SLAD"+tracknum+"_"+segnum);
                String json = objectMapper.writeValueAsString(punkty_sladu);
                System.out.println("przetworzony");
                out.print(json);

                try{
                    System.out.println("przedkopia");
                    //PunktSladu kopia = objectMapper.readValue(json, PunktSladu.class);
                    PunktySladu kopia = new PunktySladu();
                    boolean b =  kopia.zJSONA(objectMapper, json);
                    System.out.println("zakopia"+b);
                    out.println("<p>KOPIA:" +nazwa_sladu+" punkty:<br>");
                    String json2 = objectMapper.writeValueAsString(kopia);
                    System.out.println("przetworzona kopia");
                    out.print(json2);
                }catch(Exception e)
                {
                    e.printStackTrace(out);
                }

                segnum+=1;
            }
            tracknum+=1;
        }
        System.out.println("KONIEC");
        out.println("<br>KONIEC");
    }

}
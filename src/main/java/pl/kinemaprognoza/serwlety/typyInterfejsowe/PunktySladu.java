package pl.kinemaprognoza.serwlety.typyInterfejsowe;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Objects;

//To mak byc przekazywanie JSONEM, więc posyłanie tablicy wielu PunktSladu oznaczałoby, że wiekszość tego JSONA byłaby pełna napisów "lat" "lon", "elev"....
public class PunktySladu {
    public double[] lat = null;
    public double[] lon = null;
    public double[] elev = null;

    public PunktySladu(double[] lat, double[] lon, double[] elev) {
        this.lat = lat;
        this.lon = lon;
        this.elev = elev;//jeśli nie ma, to Math.NaN
    }
    public PunktySladu(){}


    //doJSONA - writevalueasString

    // Ten wielki jak hipopotam Jackson nie potrafi sobie poradzić z trzema tablicami doubli w obiekcie....
    //PunktSladu kopia = objectMapper.readValue(json, PunktSladu.class); - NIE DZIAŁA
    //więc powstało poniższe wynaturzenie
    public boolean zJSONA(ObjectMapper objectMapper, String json)
    {
        try {
            System.out.println("przedkopia");
            //PunktSladu kopia = objectMapper.readValue(json, PunktSladu.class);
            JsonNode node = objectMapper.readTree(json);
            JsonNode lat_node = node.get("lat");
            JsonNode lon_node = node.get("lon");
            JsonNode elev_node = node.get("elev");

            if(lat_node.size() != lon_node.size()){System.err.println("PunktySladu:JSON ZEPSUTY, długosc lat[] != długość lon[]"); return false;}

            lat = new double[lat_node.size()];
            lon = new double[lon_node.size()];
            elev = new double[elev_node.size()];
            for(int i=0; i<elev_node.size();i++){elev[i] = Double.NaN;}//inicjalizacja Nanami

            for (int i = 0; i < lat_node.size(); i++) {
                lat[i] = lat_node.get(i).doubleValue();
                lon[i] = lon_node.get(i).doubleValue();
            }
            for(int i=0; i<elev_node.size(); i++)
            {
                if(elev_node.get(i).textValue() != null && elev_node.get(i).textValue().equalsIgnoreCase("NaN")){elev[i] = Double.NaN;}
                else{elev[i] = elev_node.get(i).doubleValue();}
            }
            //out.print(json2);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return false;
        }
        return true;
    }
}

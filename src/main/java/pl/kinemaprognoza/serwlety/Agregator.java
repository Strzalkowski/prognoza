package pl.kinemaprognoza.serwlety;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.jenetics.jpx.GPX;
import io.jenetics.jpx.Track;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;


import javax.swing.text.Segment;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.PunktPogody;
import pl.kinemaprognoza.orm.rekordy.PunktSladu;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;
import pl.kinemaprognoza.serwlety.typyInterfejsowe.PunktySladu;
import pl.kinemaprognoza.util.UTIL;
import pl.kinemaprognoza.util.Zasysacz;

public class Agregator extends HttpServlet {
    ObjectMapper objectMapper = null;

    @Override
    public void init() throws ServletException {
        super.init();
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("DoubleSerializer", new Version(1, 0, 0, ""));

        module.addSerializer(Double.class, new NumberSerializers.DoubleSerializer(Double.class));
        objectMapper.registerModule(module);
        //objectMapper.registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
    public void agregacja_wykresu(double startlat, double startlon, long czas_start, String sid ,String hasz,HttpServletRequest request, HttpServletResponse response, Baza baza)throws ServletException, IOException
    {
        double srednia_predkosc = 15.0;//km/h
        Zasysacz zasysacz = new Zasysacz(baza, getServletContext().getRealPath("/WEB-INF/"), sid);

        List<PunktPogody> wykres = new ArrayList<>();

        try {
            PrintWriter out = response.getWriter();;

            //PrintStream out = new java.io.NullWriter();
            String json_PunktySladu = baza.dajPunktySladu(hasz);
            PunktySladu slad = new PunktySladu();
            //spróbuj parsowac JSONa z bazy
            if (!slad.zJSONA(objectMapper, json_PunktySladu) || slad.lat.length < 1 || slad.lon.length < 1) {
                UTIL.powiadomienie_o_bledzie(request, response, "JSON PRZECHOWYWANY W BAZIE OKAZAŁ SIĘ BYĆ ZEPSUTY");
                return;
            }
            double lat[] = slad.lat;
            double lon[] = slad.lon;

            //long licznik_czasu = czas_start;
            double czas_od_poczatku = 0.0;
            double licznik_odleglosci = 0.0;

            long pierwsza_pelna_godzina = UTIL.epochPoprzedniaPelnaGodzina(czas_start);

            /////DODAWANIE PUNKTU///////
            PunktPogody p = zasysacz.dajPunktPogody(lon[0], lat[0], pierwsza_pelna_godzina, baza);
            //p.czas_punktu to normalnie to, co jest w prognozie (co 3 godziny), więc trzeba przestawić na co godzinę
            p.czas_punktu = pierwsza_pelna_godzina;
            p.x = lon[0];//żeby było na śladzie, a nie w rzeczywistym miejscu z bazy
            p.y = lat[0];
            wykres.add(p);
            //out.println("zasysacz.dajPunktPogody("+lon[0]+", "+lat[0]+", "+pierwsza_pelna_godzina+");<br>");

            long nastepna_pelna_godzina = pierwsza_pelna_godzina + 3600;

            double limit_czasu = 3600*10;//nie wiecej jak 10 godz.

            for (int i = 0; i < lat.length-1 && czas_od_poczatku < limit_czasu; i++)
            {

                double odl_do_nastepnego = UTIL.odległośćSferyczna(lon[i], lat[i], lon[i+1], lat[i+1]);
                double czas_do_nastepnego = (int)(odl_do_nastepnego / srednia_predkosc * 3600);
                //out.println("szer:"+lat[i]+" dł:"+lon[i]+" odl do nast:"+odl_do_nastepnego+" czas do nast:"+czas_do_nastepnego+" licznik odl:"+licznik_odleglosci+" czas:"+czas_od_poczatku+"<br>");

                long czas_teraz = czas_start + ((long)(czas_od_poczatku));
                long czas_nast = czas_start + ((long)(czas_od_poczatku + czas_do_nastepnego));

                if(czas_nast > nastepna_pelna_godzina)
                {

                    //nowy punkt
                    //średnia ważona pomiędzy teraźniejszym a następnym
                    double xpunktu = (lon[i]*(nastepna_pelna_godzina - czas_teraz) + lon[i+1]*(czas_nast - nastepna_pelna_godzina))/(czas_nast - czas_teraz);
                    double ypunktu = (lat[i]*(nastepna_pelna_godzina - czas_teraz) + lat[i+1]*(czas_nast - nastepna_pelna_godzina))/(czas_nast - czas_teraz);

                    /////DODAWANIE PUNKTU///////
                    PunktPogody pk = zasysacz.dajPunktPogody(xpunktu, ypunktu, nastepna_pelna_godzina, baza);

                    pk.x = xpunktu;
                    pk.y = ypunktu;
                    //p.czas_punktu to normalnie to, co jest w prognozie (co 3 godziny), więc trzeba przestawić na co godzinę
                    pk.czas_punktu = nastepna_pelna_godzina;

                    //pk.numer_punktu = i;
                    wykres.add( pk );
                    //out.println("|"+czas_teraz+"|"+nastepna_pelna_godzina+"|"+czas_nast+"<br>");
                    //out.println("zasysacz.dajPunktPogody("+xpunktu+", "+ypunktu+", "+nastepna_pelna_godzina+");<br>");
                    nastepna_pelna_godzina +=3600;
                }

                licznik_odleglosci +=odl_do_nastepnego;
                czas_od_poczatku +=czas_do_nastepnego;
            }
            //out.println("<p><h2>długość wykresu:"+wykres.size()+"</h2>");
            String json_Wykres = objectMapper.writeValueAsString(wykres);
            //out.println("json:<br>"+json_Wykres);
            response.setContentType("application/json");
            out.write(json_Wykres);
        }
        catch(IOException t)
        {
            t.printStackTrace();
            return;
        }



    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        System.out.println("AGREGATOR");
        //argumenty
        String sloclat = request.getParameter("loclat");
        String sloclon = request.getParameter("loclon");
        String hasz = request.getParameter("hasz");
        String sid = request.getParameter("sid");

        System.out.println(request.getMethod());
        try {
            if ( (hasz == null || sid == null) && "POST".equalsIgnoreCase(request.getMethod()) )//&& request.getContentType() != null && request.getContentType() == "application/json")
            {
                System.out.println("JSON!!!::::\n");
                String body  = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
                System.out.println(body);
                JsonNode root = objectMapper.readTree(body);
                sid = root.get("sid").asText("");
                hasz = root.get("hasz").asText("");
            }
        }
        catch(Throwable t){t.printStackTrace(); System.out.println("BLAD PARSOWANIA JSONA PRZYCHODZACEGO");}
        System.out.println("sid"+sid);
        System.out.println("hasz"+hasz);

        double loclat = Double.NaN;
        double loclon = Double.NaN;

        //inicjalizacja i autoryzacja
        Baza baza = new Baza();
        Uzytkownik uzytkownik =  UTIL.autoryzacja(sid,request, response,baza);
        if(uzytkownik == null){return;/*autoryzacja juz jakis tam komunikat dopisała*/}
        if(!UTIL.autoryzacja_do_hasza(sid,hasz,request, response, baza)){return;}
        long czas_start = System.currentTimeMillis() / 1000L;

        agregacja_wykresu(Double.NaN, Double.NaN, czas_start, sid, hasz,  request, response, baza);

        System.out.println("AGREGATOR:KONIEC");
        baza.rozłącz();
    }


}
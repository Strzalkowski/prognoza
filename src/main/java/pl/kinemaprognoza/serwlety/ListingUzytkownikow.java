package pl.kinemaprognoza.serwlety;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;

import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;

public class ListingUzytkownikow extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //argumenty
        String sid = request.getParameter("sid");
        String typ = request.getParameter("typ");

        //
        response.setContentType("text/html");
        PrintWriter wy = response.getWriter();
        wy.print("<h1>LISTA UŻYTKOWNIKÓW</h1><br>");

        //połącznie z bazą:
        Baza baza = new Baza();
        ArrayList<Uzytkownik> uzytkownicy =  baza.dajListęUzytkownikow();
        wy.printf("liczba użytkowników:%d<br>", uzytkownicy.size());
        wy.printf("<table border=2>\n");
        wy.printf("<thead><tr><td>id</td><td>nazwa</td><td>email</td></tr></thead>\n");
        wy.printf("<tbody>");
        for(var u : uzytkownicy)
        {
            wy.printf("<tr><td>%d</td> <td>%s</td> <td>%s</td> </tr>", u.id, u.nazwa, u.email);
        }
        wy.printf("</tbody></table>\n");
        wy.print("KONIEC");
        if(baza.dajOstatniBłąd() != null){baza.dajOstatniBłąd().printStackTrace(wy);}
        baza.rozłącz();
    }

}
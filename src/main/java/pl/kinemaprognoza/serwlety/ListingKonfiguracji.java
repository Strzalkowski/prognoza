package pl.kinemaprognoza.serwlety;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.Konfiguracja;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;
import pl.kinemaprognoza.util.UTIL;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class ListingKonfiguracji  extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //argumenty
        String sid = request.getParameter("sid");
        String typ = request.getParameter("typ");

        //inicjalizacja i autoryzacja
        Baza baza = new Baza();
        Uzytkownik uzytkownik =  UTIL.autoryzacja(request, response,baza);
        if(uzytkownik == null){return;/*autoryzacja juz jakis tam komunikat dopisała*/}

        //
        response.setContentType("text/html");
        PrintWriter wy = response.getWriter();
        wy.print("<h1>LISTING TRAS WŁASNYCH</h1><br>");
        wy.print("<p>nazwa użytkownika:"+uzytkownik.nazwa+"</p>");


        ArrayList<Konfiguracja> konfiguracje = baza.dajListeKonfiguracji(uzytkownik.id);
        //wy.printf("długośc tablicy:%d<br>", konfiguracje.size());
        wy.print("<table border='1'>");
        wy.printf("<thead><tr><th>link</th><th>usuń</th></tr></thead>\n");
        wy.printf("<tbody>");
        for(var k : konfiguracje)
        {
            wy.printf("<tr><td> <a href=\"pokaz.html?hasz=%s\">%s</a> </td>  <td><a href=\"#\" class='aUsun'>usuń</a></td> </tr>", k.hasz, k.nazwakonfiguracji);
        }
        wy.printf("</tbody></table>\n");
        wy.println("<a href=\"importowanie.html\">ZAIMPORTUJ NOWY ŚLAD</a>");
        //wy.print("KONIEC");
        if(baza.dajOstatniBłąd() != null){baza.dajOstatniBłąd().printStackTrace(wy);}
        baza.rozłącz();
    }

}
package pl.kinemaprognoza.serwlety;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.jenetics.jpx.GPX;
import io.jenetics.jpx.Track;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;


import javax.swing.text.Segment;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.PunktSladu;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;
import pl.kinemaprognoza.serwlety.typyInterfejsowe.PunktySladu;
import pl.kinemaprognoza.util.UTIL;

//@WebServlet(name = "Importer", value = "/Importer")
//@WebServlet(name = "FileUploadServlet", urlPatterns = { "/fileuploadservlet" })
//https://docs.oracle.com/javaee/6/tutorial/doc/glraq.html
//https://stackoverflow.com/questions/34887382/whats-the-difference-between-doget-dopost-and-processrequest
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class Importer extends HttpServlet {
    ObjectMapper objectMapper = null;

    @Override
    public void init() throws ServletException {
        super.init();
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("DoubleSerializer", new Version(1, 0, 0, ""));

        module.addSerializer(Double.class, new NumberSerializers.DoubleSerializer(Double.class));
        objectMapper.registerModule(module);
        //objectMapper.registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /* Receive file uploaded to the Servlet from the HTML5 form */
        /*Part filePart = request.getPart("file");
        String fileName = filePart.getSubmittedFileName();
        for (Part part : request.getParts()) {
            part.getInputStream();
            //part.write("C:\\upload\\" + fileName);
        }
        */
        //inicjalizacja i autoryzacja
       Baza baza = new Baza();
       Uzytkownik uzytkownik =  UTIL.autoryzacja(request, response,baza);
       if(uzytkownik == null){return;/*autoryzacja juz jakis tam komunikat dopisała*/}

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String nazwap_uzytk = request.getParameter("nazwap"); // Retrieves <input type="text" name="nazwap">
        out.print("NAZWA OD UZYTKOWNIKA:"+nazwap_uzytk+"<br>\n");
        boolean podmieniac = request.getParameter("podmieniac").equalsIgnoreCase("true");
        out.println("PODMIENIANIE:"+podmieniac+"<br>");
        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">

        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        out.print("NAZWA PLIKU:"+fileName+"<br>\n");

        //InputStream zawartoscPliku = filePart.getInputStream();


        /*StringBuilder zawartosc = new StringBuilder();
        byte[] buf = new byte[8192];
        int length;
        while ((length = zawartoscPliku.read(buf)) > 0) {
            //out.write(String.valueOf(buf), 0, length);
            //out.print(Arrays.toString(buf));
            //sb.append(buf);
            zawartosc.append(new String(buf, StandardCharsets.UTF_8));
        }
        out.print(zawartosc.toString());
        out.print("<br>KONIEC3<br><p>");
        */

        /*
        final GPX gpx = GPX.builder()
                .addTrack(track -> track
                        .addSegment(segment -> segment
                                .addPoint(p -> p.lat(48.2081743).lon(16.3738189).ele(160))
                                .addPoint(p -> p.lat(48.2081743).lon(16.3738189).ele(161))
                                .addPoint(p -> p.lat(48.2081743).lon(16.3738189).ele(162))))
                .build();
        out.print( GPX.writer().toString(gpx) );
        */
        /////////////////
        //czytanie GPX///
        //////////////////

        //Part z requesta
        InputStream zawartoscPliku = filePart.getInputStream();
        //jeśli użytkownik nie podał nazwy, to trzeba użyć oryginalnej
        if(nazwap_uzytk == null || nazwap_uzytk.equals("")) {nazwap_uzytk =  filePart.getSubmittedFileName();}

        /*GPX.read(zawartoscPliku).tracks()
                .flatMap(Track::segments)
                .flatMap(TrackSegment::points)
                .forEach(out::println);
*/
        //lista tracków
        List<Track> tracks = GPX.read(zawartoscPliku).getTracks();
        //dla każdego tracka:
        int tracknum=0;
        for(Track track : tracks)
        {
            //wyciągnij nazwę tracka
            Optional<String> trackname_optional_wrapper_for_gods_know_what = track.getName();
            String trackname = null;
            if(trackname_optional_wrapper_for_gods_know_what.isPresent()){trackname=trackname_optional_wrapper_for_gods_know_what.get();}
            if(trackname == null){trackname = ""+tracknum;}

            //dla każdego segmentu
            int segnum=0;
            List<TrackSegment> segments = track.getSegments();
            for(TrackSegment segment : segments)
            {
                //(segmenty nie mają nazw więc tylko numerek)
                //nazwa punktów: nazwa_pliku/nazwa_podana_przez_uzytkownika_nazwa_tracka/numer_tracka_numer_segmentu
                //(albo bez nazwy tracka inumeru segmentu, jeśli jest tylko jeden track z jednym segmentem)
                List<WayPoint> points = segment.getPoints();
                String nazwa_sladu;
                if(tracks.size()==1 && segments.size()==1){nazwa_sladu = nazwap_uzytk;}
                else
                {
                    nazwa_sladu = nazwap_uzytk+"_"+((trackname == null)?(trackname):(""+tracknum))+"_"+segnum;
                }

                //zaczytaj punkty do obiektu typu PunktyŚladu
                PunktySladu punkty_sladu = new PunktySladu();
                punkty_sladu.lat = new double[points.size()];
                punkty_sladu.lon = new double[points.size()];
                punkty_sladu.elev = new double[points.size()];

                int i=0;
                for(WayPoint point :points)
                {
                    //PunktSladu p = new PunktSladu();
                    //p.lat = point.getLatitude().doubleValue();
                    //p.lon = point.getLongitude().doubleValue();
                    //p.elev= ((point.getElevation().isPresent())?(point.getElevation().get().doubleValue()):(null));
                    //punkty_sladu.add(p);
                    punkty_sladu.lat[i] = point.getLatitude().doubleValue();
                    punkty_sladu.lon[i] = point.getLongitude().doubleValue();
                    punkty_sladu.elev[i] = ((point.getElevation().isPresent())?(point.getElevation().get().doubleValue()):(Double.NaN));
                    i+=1;
                }

                //out.println("<p>NAZWA SLADU:" +nazwa_sladu+" punkty:<br>");
                //System.out.println("SLAD"+tracknum+"_"+segnum);
                String json_PunktySladu = objectMapper.writeValueAsString(punkty_sladu);
                //System.out.println("przetworzony");
                //out.print(json_PunktySladu);

                // [FANFARY] wreszcie dodanie do bazy [FANFARY]
                //out.println("DODAJ KONFIGURACJE"+nazwa_sladu+"<br>");
                //System.out.println("DODAJ KONFIGURACJE"+nazwa_sladu);
                baza.dodajKonfiguracje(json_PunktySladu, nazwa_sladu, uzytkownik, podmieniac);
//
//                try{
//                    System.out.println("przedkopia");
//                    //PunktSladu kopia = objectMapper.readValue(json, PunktSladu.class);
//                    PunktySladu kopia = new PunktySladu();
//                    boolean b =  kopia.zJSONA(objectMapper, json_PunktySladu);
//                    System.out.println("zakopia"+b);
//                    out.println("<p>KOPIA:" +nazwa_sladu+" punkty:<br>");
//                    String json2 = objectMapper.writeValueAsString(kopia);
//                    System.out.println("przetworzona kopia");
//                    out.print(json2);
//                }catch(Exception e)
//                {
//                    e.printStackTrace(out);
//                }

                segnum+=1;
            }
            tracknum+=1;
        }
        System.out.println("KONIEC");
        out.println("<br>KONIEC");
        response.sendRedirect(request.getContextPath() + "/lista.html");
        baza.rozłącz();
    }

}
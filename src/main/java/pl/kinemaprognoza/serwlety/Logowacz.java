package pl.kinemaprognoza.serwlety;


import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;

import pl.kinemaprognoza.orm.Baza;
import pl.kinemaprognoza.orm.rekordy.Sesja;
import pl.kinemaprognoza.orm.rekordy.Uzytkownik;
import pl.kinemaprognoza.util.UTIL;

public class Logowacz extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //argumenty
        String nazwa = request.getParameter("nazwa");
        String haslo = request.getParameter("haslo");

        //
        response.setContentType("text/html");
        PrintWriter wy = response.getWriter();
        wy.print("LOGOWACZ<br>");
        wy.print("nazwa:" + nazwa + "<br>");
        wy.print("haslo:" + haslo + "<br>");

        //połącznie z bazą:
        Baza baza = new Baza();
        Uzytkownik uzytkownik = baza.dajUzytkownika(nazwa, haslo);
        if (uzytkownik == null) {
            wy.print("LOGOWANIE SIE NIE POWIODŁO<br>");
            baza.rozłącz();
            return;
        } else {
            wy.print("ZNALAZŁEM UZYTKOWNIKA!<br>");
            wy.print("email:" + uzytkownik.email + "<br>");
            wy.print("haslo:" + uzytkownik.haslo + "<br>");

            Sesja sesja = baza.nowaSesja(uzytkownik);
            if(sesja==null){
                wy.print("BŁĄD DODAWANIA SESJI");
                baza.rozłącz();
                return;
            }
            wy.print("NIE KONIEC<br>");
            //Cookie cookie_sid = new Cookie("sid", sesja.sid);
            //cookie_sid.setMaxAge(0);
            //cookie_sid.setSecure(true);
            //response.addCookie(cookie_sid);
            response.addHeader("Set-Cookie", "sid="+sesja.sid+"; SameSite=Lax ");
            wy.print("USTAWIAM cookie sid:"+sesja.sid+"<br>");
            wy.print("KONIEC<br>");
            baza.rozłącz();
            response.sendRedirect(request.getContextPath() + "/lista.html");
            return;
        }
    }


}
